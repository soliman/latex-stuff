# lifewareslides.cls

Please note that there is now an [official beamer theme](https://gitlab.inria.fr/gabarits/latex-beamer-2024) for Inria slides.
The class here is undergoing some updates to fit with the new graphical charter without being too ugly, but it is not stable.

## Philosophy

This is my purely personal take on making decent slides with LaTeX.

This repo contains my XeLaTeX class for Inria-compatible slides.
Look at the source or ask me if you have any question.

- based on beamer (reference LaTeX class for slides, read its manual, it is
  really *good*)
- uses [Inria typography
  fonts](https://partage.inria.fr/share/page/document-details?nodeRef=workspace://SpacesStore/e019c816-3985-4c64-909f-909b1080eba6)
  (you may need to delete the web subdirectory…)
- and Linux [Libertine](http://libertine-fonts.org/download/)
  [fonts](https://lifeware.inria.fr/wiki/Internal/IHatePowerPoint?action=download&upname=LinLibertineOTF_5.3.0_2012_07_02.tgz)
- uses the official [Inria
  Logo](https://numin.inria.fr/portal/g/:spaces:71lwaa/base_de_connaissance/notes/3805?translation=en)
- relies on Courier New (so you might need to `sudo apt-get install
  font-manager ttf-mscorefonts-installer`)
- use XeLaTeX for compilation instead of pdflatex to get better fonts and
  out-of-the-box support for Unicode (so use `xelatex slides.tex` or even
  better `latexmk -xelatex slides.tex` to compile)
- LuaLaTeX is also supported, and will provide `\emoji{shrug}` support

To install the fonts on Ubuntu machines, just copy the TTF to
`/usr/share/fonts/truetype` or the OTF to `/usr/share/fonts/opentype`
(alternatively `~/.fonts/`) and maybe recreate the font cache with
`sudo fc-cache -f -v`.

## Example
Here is an example of use of that class: [the
source](https://lifeware.inria.fr/wiki/Internal/IHatePowerPoint?action=download&upname=InternalSeminar180410_soule.tex) of my Internal Seminar talk of 2018/04/10

- includes some tikz-based graphing
- includes some full-slide images (sources not given… sorry)
- relies on the provided biocham-mode for listings
- might be obsolete with respect to the latest Inria charter colors

## Tips

The following options are supported:

- `aspectratio` (passed to beamer, defaults to `169`)
- `code`: include the `listings` package with decent defaults
- `nag`: include the `nag` package
- `notes`: generate notes for each slide (see section 19 of beamer user guide)
- `french`: include the corresponding `babel` package
- `blocks`: re-enable blocks (like `exampleblock`) around theorems
  (disabled by default)

Note that `tikz` is now always included (in order to obtain the nice Inria gradient).

The following colors are defined:
```latex
\definecolor{inriarouge}{RGB}{201,25,30}
\definecolor{inriaframboise}{RGB}{166,15,121}
\definecolor{inriaviolet}{RGB}{93,75,154}
\definecolor{inriabnuit}{RGB}{39,52,139}
\definecolor{inriabcanard}{RGB}{16,103,163}
\definecolor{inriagrisbleu}{RGB}{56,66,87}
% avoid the following ones on a white background
\definecolor{inriabazur}{RGB}{0,164,202}
\definecolor{inriabvert}{RGB}{136,205,202}
\definecolor{inriacactus}{RGB}{96,139,55}
\definecolor{inriavtendre}{RGB}{149,193,31}
\definecolor{inriajaune}{RGB}{255,205,28}
\definecolor{inriaorange}{RGB}{255,131,0}
\definecolor{inriasable}{RGB}{214,188,134}
```

You need to use the class option `oldcolors` to use the deprecated colors from the previous charter.

You can use the command `\pictureframe{pic.jpg}{contents}` to create a slide
with a picture as background (set to height).

In the `vitemize` environment, the length `itemsep` is set to `\fill` to add an automatic `\vfill` between items.

## Copyright and license

Copyright Sylvain Soliman

This work may be distributed and/or modified under the
conditions of the LaTeX Project Public License, either version 1.3
of this license or (at your option) any later version.
The latest version of this license is in
  https://www.latex-project.org/lppl.txt
and version 1.3c or later is part of all distributions of LaTeX
version 2008 or later.

This work has the LPPL maintenance status `maintained'.

The Current Maintainer of this work is Sylvain Soliman
