% Copyright Sylvain Soliman
%
% This work may be distributed and/or modified under the
% conditions of the LaTeX Project Public License, either version 1.3
% of this license or (at your option) any later version.
% The latest version of this license is in
%   https://www.latex-project.org/lppl.txt
% and version 1.3c or later is part of all distributions of LaTeX
% version 2008 or later.
%
% This work has the LPPL maintenance status `maintained'.
%
% The Current Maintainer of this work is Sylvain Soliman
\NeedsTeXFormat{LaTeX2e}[2017/04/15]
\ProvidesClass{lifewareslides}[2024/11/15 v2.1 Pragmatic beamer template for the
Lifeware team trying to abide to the graphical chart]

\RequirePackage{xkeyval}
\DeclareOptionX{aspectratio}[169]{\PassOptionsToClass{aspectratio=#1}{beamer}}
\DeclareOptionX{handout}{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptionsX\relax%

\LoadClass[14pt]{beamer}

\newif\ifcode\codefalse%
\DeclareOption{code}{\codetrue}
\newif\ifnag\nagtrue%
\DeclareOption{nag}{\nagfalse}
\newif\ifnotes\notesfalse%
\DeclareOption{notes}{\notestrue}
\newif\iffrench\frenchfalse%
\DeclareOption{french}{\frenchtrue}
\newif\ifblock\blockfalse%
\DeclareOption{blocks}{\blocktrue}
\newif\ifoldcolors\oldcolorsfalse%
\DeclareOption{oldcolors}{\oldcolorstrue}
\ProcessOptions\relax%

\RequirePackage{iftex}
\iffrench%
  \iftutex% all Unicode engines, XeTeX LuaTeX, etc.
    \RequirePackage{polyglossia}\setdefaultlanguage{french}\else\RequirePackage[french]{babel}\fi
\fi
\ifnag\RequirePackage{nag}\fi

\RequirePackage[only,llbracket,rrbracket,Arrownot]{stmaryrd}
\RequirePackage{graphicx}
\RequirePackage{xspace}
\RequirePackage{microtype}
\RequirePackage[math-style=ISO,bold-style=ISO]{unicode-math}
\RequirePackage{booktabs}  % TODO use tabularray instead of booktabs+siunitx
\RequirePackage{siunitx}   % table column S to aling on dot

\ifXeTeX%
  \RequirePackage{xltxtra}
  \newcommand{\emoji}[1]{\ClassWarning{lifewareslides.cls}{emoji support requires LuaTeX}}
\else
  \ifLuaHBTeX%
    \RequirePackage{fontspec}
    \RequirePackage{emoji}
  \else
    \ClassWarning{lifewareslides.cls}{requires XeTeX or LuaTeX}
  \fi
\fi

\RequirePackage{xcolor}
% \definecolor{green}{rgb}{0.0,0.65,0.0}
\definecolor{inriarouge}{RGB}{201,25,30}
\definecolor{inriaframboise}{RGB}{166,15,121}
\definecolor{inriaviolet}{RGB}{93,75,154}
\definecolor{inriabnuit}{RGB}{39,52,139}
\definecolor{inriabcanard}{RGB}{16,103,163}
\definecolor{inriagrisbleu}{RGB}{56,66,87}
% avoid the following ones on a white background
\definecolor{inriabazur}{RGB}{0,164,202}
\definecolor{inriabvert}{RGB}{136,205,202}
\definecolor{inriacactus}{RGB}{96,139,55}
\definecolor{inriavtendre}{RGB}{149,193,31}
\definecolor{inriajaune}{RGB}{255,205,28}
\definecolor{inriaorange}{RGB}{255,131,0}
\definecolor{inriasable}{RGB}{214,188,134}
% DEPRECATED
\ifoldcolors
  \definecolor{inriablue}{RGB}{56,66,87}
  \definecolor{inriavert}{RGB}{149,193,31}
  \definecolor{inriamauve}{RGB}{101,97,169}
  \definecolor{inrialilas}{RGB}{155,0,79}
  \definecolor{inriavertclair}{RGB}{199,214,79}
  \definecolor{inriableuclair}{RGB}{137,204,202}
  \definecolor{inriableu}{RGB}{20,136,202}
  \definecolor{inriagrisclair}{RGB}{230,231,232}
\fi

% Solarized colors after https://github.com/jez/latex-solarized
\definecolor{sbase03}{HTML}{002B36}
\definecolor{sbase02}{HTML}{073642}
\definecolor{sbase01}{HTML}{586E75}
\definecolor{sbase00}{HTML}{657B83}
\definecolor{sbase0}{HTML}{839496}
\definecolor{sbase1}{HTML}{93A1A1}
\definecolor{sbase2}{HTML}{EEE8D5}
\definecolor{sbase3}{HTML}{FDF6E3}
\definecolor{syellow}{HTML}{B58900}
\definecolor{sorange}{HTML}{CB4B16}
\definecolor{sred}{HTML}{DC322F}
\definecolor{smagenta}{HTML}{D33682}
\definecolor{sviolet}{HTML}{6C71C4}
\definecolor{sblue}{HTML}{268BD2}
\definecolor{scyan}{HTML}{2AA198}
\definecolor{sgreen}{HTML}{859900}

\ifcode%
   \RequirePackage{listings}
   \lstdefinestyle{Python}{%
      language=Python,
      showtabs=true,
      showstringspaces=false,
      tabsize=4,
      morestring=[b][keywordstyle2]{__}
   }
   \lstset{%
     % How/what to match
     sensitive=true,
     % Border (above and below)
     frame=lines,
     % Extra margin on line (align with paragraph)
     xleftmargin=\parindent,
     % Put extra space under caption
     belowcaptionskip=1\baselineskip,
     % Colors
     backgroundcolor=\color{sbase3},
     basicstyle=\color{sbase00}\ttfamily,
     keywordstyle=\color{scyan},
     commentstyle=\color{sbase1},
     stringstyle=\color{sblue},
     numberstyle=\color{sviolet},
     identifierstyle=\color{sbase00},
     % Break long lines into multiple lines?
     breaklines=true,
     % Show a character for spaces?
     showstringspaces=false,
     tabsize=2
   }
   % \lstset{%
   %    commentstyle=\color{inriavert},
   %    stringstyle=\color{magenta},
   %    keywordstyle=\color{inrialilas},
   %    keywordstyle={[2]\color{inriaorange}},
   %    emphstyle=\color{inriarouge},
   %    language=biocham,
   %    basicstyle=\ttfamily,
   %    keepspaces=true,
   % }
\fi

\RequirePackage{tikz}
\usetikzlibrary{arrows.meta,calc,petri,positioning,shapes,shadows,graphs}
%,decorations.pathmorphing,backgrounds,fit}
\tikzset{%
  every place/.style={draw=inriaorange!70,fill=inriaorange!20,thick,
    minimum size=8mm},
  every transition/.style={draw=inriamauve!50,fill=inriamauve!20,thick,
    minimum size=6mm},
  pre/.style={<-,shorten <=1pt,>=stealth,thick},
  post/.style={->,shorten >=1pt,>=stealth,thick},
  round/.style={rounded corners=5pt},
  fire/.style={transition,fill=yellow},
  posreg/.style={->,shorten >=1pt,>=stealth,very thick,green},
  negreg/.style={-|,shorten >=1pt,>=stealth,very thick,red},
  ampersand replacement=\&,
  % https://tex.stackexchange.com/questions/55806/mindmap-tikzpicture-in-beamer-reveal-step-by-step/55849#55849
  invisible/.style={opacity=0},
  visible on/.style={alt={#1{}{invisible}}},
  alt/.code args={<#1>#2#3}{%
    \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
  },
}

\ifnotes%
  % % generate notes for each slide
  \makeatletter
  \def\beamer@framenotesbegin{% at beginning of slide
    \gdef\beamer@noteitems{}%
    \gdef\beamer@notes{{}}% used to be totally empty.
  }
  \makeatother
  % \setbeameroption{show only notes}
\fi

\usefonttheme{professionalfonts}
% \defaultfontfeatures[\rmfamily,\sffamily]{Ligatures=TeX}  % that is already the default
\setmathfont{Asana-Math.otf}[Ligatures=TeX] % provided by unicode-math
\setsansfont{Inria Sans}[SmallCapsFont={Linux Libertine O}]
\setromanfont{Inria Serif}
\IfFontExistsTF{Hack}{%
  \setmonofont{Hack}[Scale=0.9]}{%
  \setmonofont{Courier New}[FakeBold=1.2]
}

%%% https://tex.stackexchange.com/questions/55664/fake-small-caps-with-xetex-fontspec
\newcommand\fauxsc[1]{\textrm{\fauxschelper#1 \relax\relax}}
\def\fauxschelper#1 #2\relax{%
  \fauxschelphelp#1\relax\relax%
  \if\relax#2\relax\else\ \fauxschelper#2\relax\fi%
}
\def\Hscale{.83}\def\Vscale{.72}\def\Cscale{1.00}
\def\fauxschelphelp#1#2\relax{%
  \ifnum`#1>``\ifnum`#1<`\{\scalebox{\Hscale}[\Vscale]{\uppercase{#1}}\else%
    \scalebox{\Cscale}[1]{#1}\fi\else\scalebox{\Cscale}[1]{#1}\fi%
  \ifx\relax#2\relax\else\fauxschelphelp#2\relax\fi}
%%%

\usetheme{Boadilla}
\setbeamertemplate{footline}{%
   \hfill
   \begin{beamercolorbox}[ht=2.25ex,dp=1ex,right]{}
      \insertframenumber{}
      \hspace*{1ex}
   \end{beamercolorbox}
}
\setbeamertemplate{navigation symbols}{}
\setbeamerfont{frametitle}{size=\large,family=\rmfamily}
\usecolortheme[named=inriarouge]{structure}
\setbeamercolor{normal text}{fg=inriabnuit}
\institute{%
  \includegraphics[width=4cm]{inr_logo_rouge_rvb.pdf}\\[2ex]
  \begin{tikzpicture}
    \shade [left color=inriabnuit, right color=inriarouge, middle color=inriaframboise] (0, 0) rectangle (25mm, 3pt);
  \end{tikzpicture}
}

\newcommand{\titleframe}[1]{%
   {%
      \setbeamercolor{background canvas}{bg=inriarouge}
      \begin{frame}
         \begin{center}
            \textcolor{white}{\fontsize{40}{40}\selectfont #1}
         \end{center}
      \end{frame}
      \setbeamercolor{background canvas}{bg=white}
   }
}

\newcommand{\pictureframe}[2]{%
   {%
      \usebackgroundtemplate{%
         \includegraphics[height=\paperheight]{#1}
      }
      \begin{frame}[plain]
         #2
      \end{frame}
   }
}

\newcommand{\hpictureframe}[2]{%
   {%
      \usebackgroundtemplate{%
         \includegraphics[width=\paperwidth]{#1}
      }
      \begin{frame}[plain]
         #2
      \end{frame}
   }
}

\renewcommand{\cite}[1]{{\small [#1]}}

\renewcommand\theenumi{\roman{enumi}}
\newenvironment{vitemize}{\itemize\setlength{\itemsep}{\fill}}{\enditemize} % automatic vfill between items

\ifblock%
\else%
   \setbeamertemplate{theorem begin}
   {%
      % \inserttheoremheadfont% uncomment if you want amsthm-like formatting
      {
         \small\textbf{%
            \inserttheoremname%
            % \inserttheoremnumber
         \ifx\inserttheoremaddition\@empty\else: \inserttheoremaddition\fi%
            % \inserttheorempunctuation
         }
      }
      \hskip\thm@headsep%
   }
   \setbeamertemplate{theorem end}{\par}
   \setbeamertemplate{proof begin}{{\small\textbf{Proof}}\hskip\thm@headsep}
   \setbeamertemplate{proof end}{}
\fi%

\newtheorem{proposition}{Proposition}
\newenvironment{thm}{\begin{theorem}}{\end{theorem}}
\newenvironment{prop}{\begin{proposition}}{\end{proposition}}
\newenvironment{cor}{\begin{corollary}}{\end{corollary}}
\newenvironment{defi}{\begin{definition}}{\end{definition}}
\newenvironment{lem}{\begin{lemma}}{\end{lemma}}

\endinput
